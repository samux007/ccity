# City weather Comparison

  this is a django app to let you compare weather from different cities

## REQUIREMENTS
    * django > 1.5
    * django-bootstrap3
    * api key from wunderground.com
    * dygraph js

project/urls.py contains:

        url(r'^ccity/', include('ccity.urls')),

project/settings.py contains:

INSTALLED_APPS = (
    ....
    'bootstrap3',
    'ccity',
    ....
)

### Usage
 
Choose the cities to get data from as icao code and put them inside retriever.py
launch once per day retriever, it has a setup to not go over 480 query per day
so you can use a free licence at wunderground.

use ./inserter.py script to insert them on the database 

go to /ccity/ and choose cities to compare to.
