from django import forms
from ccity.models import IcaoCode, DayData
from django.forms import ModelChoiceField

attrs={
        'class':'btn btn-default btn-sm',
        'onchange':'javascript:this.form.submit();'
        }

def getcity():
    codes = DayData.objects.values('code').distinct()
    return IcaoCode.objects.filter(icao__in=codes)


class MyModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return "%s" % obj.city

class CompareForm(forms.Form):
    city1 = MyModelChoiceField(label="", queryset=getcity(),empty_label=None, widget=forms.Select(attrs={'class':'selector'}))
    city2 = MyModelChoiceField(label="", queryset=getcity(),empty_label=None, widget=forms.Select(attrs={'class':'selector'}))
