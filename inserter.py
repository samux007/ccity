#!/public/varie/bin/python

import os
os.environ['PYTHONPATH'] = '/pathtodjango'
os.environ['DJANGO_SETTINGS_MODULE'] = 'ccity.settings'

import json
from ccity.models import getdatavalues
from ccity.models import IcaoCode
from ccity.models import DayData
from django.utils.timezone import utc
import datetime

BASEDIR='./data'

rf=open('./city_airport_codes.json','r')
jd = json.load(rf)
for val in jd:
    if len(IcaoCode.objects.filter(icao=val['icao_code']))==0:
        IcaoCode.objects.create(city=val['city'],country=val['country'],icao=val['icao_code'])

for city in os.listdir(BASEDIR):
    for files in os.listdir('%s/%s'%(BASEDIR,city)):
        rf=open('%s/%s/%s'%(BASEDIR,city,files),'r')
        data = json.load(rf)
        rf.close()
        ###for obs in data['history']['observations']:
        for obs in data['history']['dailysummary']:
            getdatavalues(city,obs)
