from django.db import models
import json
import datetime

class ApiKey(models.Model):
    key = models.CharField(max_length=24)

class IcaoCode(models.Model):
    city    = models.CharField(max_length=30)
    country = models.CharField(max_length=30)
    icao    = models.CharField(max_length=4)

class HourData(models.Model):
    code = models.CharField(max_length=4)   
    date = models.DateField()
    fog  = models.BooleanField(default=False)
    rain = models.BooleanField(default=False)
    snow = models.BooleanField(default=False)
    hail = models.BooleanField(default=False)
    thunder = models.BooleanField(default=False)
    tornado = models.BooleanField(default=False)
    wdird = models.DecimalField(max_digits=3,decimal_places=0)
    wdire = models.CharField(max_length=8)   
    pressurem  = models.DecimalField(max_digits=4,decimal_places=0)
    precipm = models.DecimalField(max_digits=3,decimal_places=2)
    vism  = models.DecimalField(max_digits=3,decimal_places=1)
    wspdm  = models.DecimalField(max_digits=2,decimal_places=0)
    heatindexm = models.DecimalField(max_digits=3,decimal_places=1)
    windchillm = models.DecimalField(max_digits=3,decimal_places=1)
    hum = models.DecimalField(max_digits=3,decimal_places=0)


class DayData(models.Model):
    code = models.CharField(max_length=4)   
    date = models.DateField()
    fog  = models.BooleanField(default=False)
    rain = models.BooleanField(default=False)
    snow = models.BooleanField(default=False)
    hail = models.BooleanField(default=False)
    thunder = models.BooleanField(default=False)
    tornado = models.BooleanField(default=False)
    mintempm =  models.DecimalField(max_digits=6,decimal_places=2)
    meantempm = models.DecimalField(max_digits=6,decimal_places=2)
    maxtempm =  models.DecimalField(max_digits=6,decimal_places=2)
    minpressurem  = models.DecimalField(max_digits=6,decimal_places=2)
    meanpressurem = models.DecimalField(max_digits=6,decimal_places=2)
    maxpressurem  = models.DecimalField(max_digits=6,decimal_places=2)
    minvism  = models.DecimalField(max_digits=4,decimal_places=1)
    meanvism = models.DecimalField(max_digits=4,decimal_places=1)
    maxvism  = models.DecimalField(max_digits=4,decimal_places=1)
    minhumidity = models.DecimalField(max_digits=4,decimal_places=1)
    humidity = models.DecimalField(max_digits=4,decimal_places=1)
    maxhumidity = models.DecimalField(max_digits=4,decimal_places=1)
    minwspdm  = models.DecimalField(max_digits=5,decimal_places=1)
    meanwindspdm = models.DecimalField(max_digits=5,decimal_places=1)
    maxwspdm = models.DecimalField(max_digits=5,decimal_places=1)
    meanwdird = models.DecimalField(max_digits=5,decimal_places=2)
    meanwdire = models.CharField(max_length=8)   
    precipm = models.DecimalField(max_digits=7,decimal_places=2)

    def __unicode__(self):
        return "%s %s"%(self.code,self.date.strftime("%Y/%m/%d"))

def getdatavalues(code,jsondata):
    dic={}
    dic['code']=code
    dic['date']=datetime.datetime(int(jsondata['date']['year']),int(jsondata['date']['mon']),int(jsondata['date']['mday']))
    for label in [u'fog',u'rain',u'snow',u'hail',u'thunder',u'tornado']:
        if jsondata.get(label) == '1':
            dic[label]=True
        else:
            dic[label]=False
    for label in [u'mintempm', u'meantempm', u'maxtempm', u'minpressurem', u'meanpressurem', u'maxpressurem', u'minvism', u'meanvism', u'maxvism', u'minhumidity', u'humidity', u'maxhumidity', u'minwspdm', u'meanwindspdm', u'maxwspdm', u'meanwdird', u'precipm']:
        if not jsondata.get(label):
            dic[label]=0
        else:
            try:
                float(jsondata.get(label))
                if float(jsondata.get(label)) < -999:
                    dic[label]=0
                else:
                    dic[label]=float(jsondata.get(label))
            except ValueError:
                dic[label]=0
    if len(DayData.objects.filter(code=code,date=dic['date'])) ==0:
        try:
            pp = DayData.objects.create(**dic)
        except Exception,e:
            print dic
            sys.exit(1)
