#!/usr/bin/python

import os
import sys
import time
import datetime
import requests
import json

key='insert wunderground key'
ic = ['KJFK','KNYC','EIDW','CYVR','KSNA','NTTB','LFPT','NZAA','KSFO','EDDT','EGLL']
TOTNUM=480

def getdata(num,icao_code):
    if not os.path.exists('data'):
        os.makedirs('data')
    os.chdir('data')
    if not os.path.exists(icao_code):
        os.makedirs(icao_code)
    os.chdir(icao_code)
    now = datetime.datetime.now()
    while ((num < TOTNUM) and (now.year > 2012)):
        datad = now.strftime('%Y%m%d')
        url='http://api.wunderground.com/api/%s/history_%s/q/%s.json'%(key,datad,icao_code)
        #print url
        wname = '%s_%s.json'%(icao_code,datad)
        if not os.path.exists(wname):
            r = requests.get(url)
            if r.status_code == 200:
                num=num+1
                print num
                data = r.text
                dj = json.loads(data)
                if dj.has_key('history'):
                    wf = open(wname,'w')
                    wf.write(data)
                    wf.close()
                else:
                    print data
                time.sleep(6)
            #sys.exit(2)
        now = now - datetime.timedelta(days=1)
    os.chdir("../..")
    return num

num = 0
for icao in ic:
    num = getdata(num,icao)
    
