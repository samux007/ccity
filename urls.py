from django.conf.urls import patterns, url

from ccity import views

urlpatterns = patterns('',
    url(r'^$', views.ccity_index, name='ccity_index')
)
