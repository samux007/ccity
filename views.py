from django.shortcuts import render
from ccity.forms import CompareForm
from ccity.models import *
from django.core.context_processors import csrf

import json
import datetime

startdate=datetime.datetime(2013,1,1)

def create_datacity(icaoid):
    data={}
    val=IcaoCode.objects.filter(id=icaoid)
    if len(val)>0:
        data['name']=val[0].city
        data['country']=val[0].country
        data['airport']=val[0].icao
        date=startdate
        values=[]
        tmp={'count':0,'fog':0,'rain':0,'snow':0,'hail':0,'thunder':0,'tornado':0, 'meantempm':0, 'meanpressurem':0, 'meanvism':0, 'humidity':0, 'meanwindspdm':0, 'precipm':0}
        while date < datetime.datetime.now():
            obj=DayData.objects.filter(code=val[0].icao,date=date)
            if ( (date - startdate).days % 7) == 0:
                if tmp:
                    values.append(date)
                    data[date]=tmp
                tmp={'count':0,'fog':0,'rain':0,'snow':0,'hail':0,'thunder':0,'tornado':0, 'meantempm':0, 'meanpressurem':0, 'meanvism':0, 'humidity':0, 'meanwindspdm':0, 'precipm':0}
            if len(obj)>0:
                tmp['count']+=1
                if obj[0].fog:
                    tmp['fog']+=1
                if obj[0].rain:
                    tmp['rain']+=1
                if obj[0].snow:
                    tmp['snow']+=1
                if obj[0].hail:
                    tmp['hail']+=1
                if obj[0].thunder:
                    tmp['thunder']+=1
                if obj[0].tornado:
                    tmp['tornado']+=1
                if obj[0].meantempm:
                    tmp['meantempm']+=obj[0].meantempm
                if obj[0].meanpressurem:
                    tmp['meanpressurem']+=obj[0].meanpressurem
                if obj[0].meanvism:
                    tmp['meanvism']+=obj[0].meanvism
                if obj[0].humidity:
                    tmp['humidity']+=obj[0].humidity
                if obj[0].meanwindspdm:
                    tmp['meanwindspdm']+=obj[0].meanwindspdm
                if obj[0].precipm:
                    tmp['precipm']+=obj[0].precipm
            date=date+datetime.timedelta(days=1)
        data['dates']=values
    return data

def ccity_index(request):
    pass
    c = {'conditions':[]}
    c.update(csrf(request))
    form = CompareForm()
    fogdata=""
    raindata=""
    precdata=""
    snowdata=""
    haildata=""
    thunderdata=""
    tornadodata=""
    tempdata=""
    humdata=""
    presdata=""
    visdata=""
    winddata=""
    if request.method == 'POST':
        form = CompareForm(request.POST)
        c['city1']=create_datacity(form.data['city1'])
        c['city2']=create_datacity(form.data['city2'])
        for dt in c['city1']['dates']:
            count1=c['city1'][dt]['count']
            count2=c['city2'][dt]['count']
            if count1==0:
                count1=1
            if count2==0:
                count2=1
            fogdata=fogdata+"%s,%f,%f\\n"%(dt.strftime("%Y-%m-%d"),c['city1'][dt]['fog']*100.0/count1,c['city2'][dt]['fog']*100.0/count2)
            raindata=raindata+"%s,%f,%f\\n"%(dt.strftime("%Y-%m-%d"),c['city1'][dt]['rain']*100.0/count1,c['city2'][dt]['rain']*100.0/count2)
            precdata=precdata+"%s,%f,%f\\n"%(dt.strftime("%Y-%m-%d"),c['city1'][dt]['precipm']/count1,c['city2'][dt]['precipm']/count2)
            snowdata=snowdata+"%s,%f,%f\\n"%(dt.strftime("%Y-%m-%d"),c['city1'][dt]['snow']*100.0/count1,c['city2'][dt]['snow']*100.0/count2)
            haildata=haildata+"%s,%f,%f\\n"%(dt.strftime("%Y-%m-%d"),c['city1'][dt]['hail']*100.0/count1,c['city2'][dt]['hail']*100.0/count2)
            thunderdata=thunderdata+"%s,%f,%f\\n"%(dt.strftime("%Y-%m-%d"),c['city1'][dt]['thunder']*100.0/count1,c['city2'][dt]['thunder']*100.0/count2)
            tornadodata=tornadodata+"%s,%f,%f\\n"%(dt.strftime("%Y-%m-%d"),c['city1'][dt]['tornado']*100.0/count1,c['city2'][dt]['tornado']*100.0/count2)
            tempdata=tempdata+"%s,%f,%f\\n"%(dt.strftime("%Y-%m-%d"),c['city1'][dt]['meantempm']/count1,c['city2'][dt]['meantempm']/count2)
            humdata=humdata+"%s,%f,%f\\n"%(dt.strftime("%Y-%m-%d"),c['city1'][dt]['humidity']/count1,c['city2'][dt]['humidity']/count2)
            visdata=visdata+"%s,%f,%f\\n"%(dt.strftime("%Y-%m-%d"),c['city1'][dt]['meanvism']/count1,c['city2'][dt]['meanvism']/count2)
            presdata=presdata+"%s,%f,%f\\n"%(dt.strftime("%Y-%m-%d"),c['city1'][dt]['meanpressurem']/count1,c['city2'][dt]['meanpressurem']/count2)
            winddata=winddata+"%s,%f,%f\\n"%(dt.strftime("%Y-%m-%d"),c['city1'][dt]['meanwindspdm']/count1,c['city2'][dt]['meanwindspdm']/count2)
        
    c['conditions'].append(['fog',fogdata])
    c['conditions'].append(['rain',raindata])
    c['conditions'].append(['precipitation',precdata])
    c['conditions'].append(['snow',snowdata])
    c['conditions'].append(['hail',haildata])
    c['conditions'].append(['thunder',thunderdata])
    c['conditions'].append(['tornado',tornadodata])
    c['conditions'].append(['humidity',humdata])
    c['conditions'].append(['wind',winddata])
    c['conditions'].append(['visibility',visdata])
    c['conditions'].append(['pressure',presdata])
    c['temperature']=tempdata
    c['form']=form
    return render(request, 'compare.html',c)
